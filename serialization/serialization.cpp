#include <iostream>
#include <fstream>
#include <set>
#include <map>
#include <unordered_map>
#include <vector>
#include <stdint.h>
#include <string.h>
#include <algorithm>

void to_bin_file(std::string outputfile, std::map<uint32_t, std::vector<std::pair<uint32_t, std::set<std::pair<uint32_t, uint8_t>>>>> adjacency){
        std::ofstream output(outputfile, std::ios_base::binary);
        int sz = 0;
        for (auto &degrees : adjacency) {
            uint32_t vertex = degrees.first;
            uint32_t count = degrees.second.size();
            output.write(reinterpret_cast<char*>(&vertex), sizeof(vertex));
            output.write(reinterpret_cast<char*>(&count), sizeof(count));
            for (auto &adjacencyr : degrees.second) {
                output.write(reinterpret_cast<char*>(&adjacencyr.first), sizeof(adjacencyr.first));
                for (auto &adj : adjacencyr.second) {
                    uint32_t node = adj.first;
                    unsigned char weight = adj.second;
                    output.write(reinterpret_cast<const char*>(&node), sizeof(node));
                    output.write(reinterpret_cast<const char*>(&weight), sizeof(weight));
                }
            }
        }
        output.close();

    }

void serialize(std::string inputfile, std::string outputfile) {
    std::ifstream input(inputfile);
    std::unordered_map<uint32_t, std::set<std::pair<uint32_t, uint32_t> > > graph;
    uint32_t source, dest, weight;
    while (input >> source >> dest >> weight) {
        graph[source].insert(std::make_pair(dest, weight));
        graph[dest].insert(std::make_pair(source, weight));
    }
    input.close();

    struct Compression {
        bool operator()(const std::pair<uint32_t, uint32_t>& lhs, const std::pair<uint32_t, uint32_t>& rhs) const {
            return (lhs.second > rhs.second) || (lhs.second == rhs.second && lhs.first < rhs.first);
        }
    };
    std::set<std::pair<uint32_t, uint32_t>, Compression> vertexset; 
    std::map<uint32_t, uint32_t> degree;
    for (auto &edge : graph) {
        degree[edge.first] = edge.second.size();
        vertexset.insert({edge.first, edge.second.size()});
    }
    std::map<uint32_t, std::vector<std::pair<uint32_t, std::set<std::pair<uint32_t, uint8_t>>>>> edges;
    while (!vertexset.empty()) {
        auto s = vertexset.begin();
        std::pair<uint32_t, uint32_t> next_v = *(s);
        vertexset.erase(s);
        std::set<std::pair<uint32_t, uint8_t>> redges;
        std::set<std::pair<uint32_t, std::pair<uint32_t, uint8_t>>> erase;
        for (auto &[vertex, weight] : graph[next_v.first]) {
            if (vertex != next_v.first) {
                erase.insert({next_v.first, {vertex, weight}});
                erase.insert({vertex, {next_v.first, weight}});
                degree[vertex]--;
                if (vertexset.find({vertex, degree[vertex]}) != vertexset.end()) {
                        vertexset.insert({vertex, degree[vertex]});
                    }
                degree[next_v.first]--;
            }
            redges.insert({vertex, weight});
        }
        for (auto &[f, s] : erase) {
            graph[f].erase(s);
        }
        if (redges.size() > 0) {
            edges[redges.size()].push_back({next_v.first, redges});
        }
    }
    to_bin_file(outputfile, edges);
}


void deserialize(std::string inputfile, std::string outputfile) {
    std::ifstream input(inputfile, std::ios_base::binary);

    std::uint32_t degree, source, dest, count;
    std::uint8_t weight;

    std::ofstream output(outputfile);
    while (input.read(reinterpret_cast<char*>(&degree), sizeof(degree))) {
        input.read(reinterpret_cast<char*>(&count), sizeof(count));
        while (count--) {
            input.read(reinterpret_cast<char*>(&source), sizeof(source));
            for (std::uint32_t i = 0; i < degree; ++i) {
                input.read(reinterpret_cast<char*>(&dest), sizeof(dest));
                input.read(reinterpret_cast<char*>(&weight), sizeof(weight));
                output << source << "\t" << dest << "\t" << (std::uint32_t)weight << "\n";
            }
        }
    }
    input.close();
    output.close();
}

int main(int argc, char **argv) {

    std::string input_file("");
    std::string output_file("");
    if (argc > 32){
        std::runtime_error("To many arguments!"); 
    }
    const std::vector<std::string> args(argv + 1, argv + argc);
    bool serialization = false;
    bool deserialization = false;
    bool _input = false;
    bool _output = false;

    for (const auto& arg : args) {
        if (arg == "-s"){
            serialization = true;
        }
        else if (arg == "-d"){
            deserialization = true;
        }
        else if (arg == "-i"){
            _input = true;
        }
        else if (arg == "-o"){
            _output = true;
        }

        else if (_input && input_file == ""){
            input_file = arg;
        }

        else if (_output && output_file == ""){
            output_file = arg;
        }
    }
    if (serialization == deserialization)
        throw std::runtime_error("Please specify workflow: either -s or -d");
    if (input_file == "" || output_file == "")
        throw std::runtime_error("Please specify both input and output files");
    
    if (serialization == true) {
        serialize(input_file, output_file);
    } else if (deserialization == true) {
        deserialize(input_file, output_file);
    }

    return 0;
}