#include <string_view>
template <char ...c> struct TString{
    static constexpr const char value[] = {c..., '\0'};
    
    };

template <typename T, T... c>
constexpr TString<c...> operator"" _s() { 
    return TString<c...>();
}

template <char ...c1, char ...c2>
constexpr TString<c1..., c2...> operator+(const TString<c1...> s1, const TString<c2...> s2) {
    return TString<c1..., c2...>();
}

template<char... c1>
constexpr const char TString<c1...>::value[];

template <char ...c1, char ...c2>
constexpr bool operator==(TString<c1...> s1, TString<c2...> s2){
    return std::string_view(TString<c1...>::value) == std::string_view(TString<c2...>::value);
}
template <char a1, char ...c1, char a2, char ...c2>
constexpr bool operator==(const TString<a1, c1...>& s1, const TString<a2, c2...>& s2) {
    return a1 == a2 && sizeof...(c1) == sizeof...(c2) && TString<c1...>() == TString<c1...>();
}
int main() {
    constexpr auto hello = "hello"_s + " world"_s;
    static_assert(hello == "hello world"_s);
    return 0;
}

