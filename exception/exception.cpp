#include <iostream>
#include <exception>
#include <stdexcept>
#include <boost/lexical_cast.hpp>
#include "gtest/gtest.h"

class TMyException : public std::exception {
public:
    TMyException() : message("Custom exception: ") {}
    explicit TMyException(const char* msg) : message(msg) {}
    explicit TMyException(const std::string& msg) : message(msg) {}

    template <typename T>
    TMyException& operator<<(const T& value) {
        message += boost::lexical_cast<std::string>(value);
        return *this;
    }

    const char* what() const noexcept override {
        return message.c_str();
    }

private:
    std::string message;
};

class TCustomException1 : public TMyException {
};

class TCustomException2 : public TMyException {
};

class Custom_test:public testing::Test {};

TEST(Custom_test, exception)
{
    TMyException Dog;
    Dog << "dog";
    EXPECT_THROW(throw Dog, TMyException);
    TCustomException1 Puppy1;
    Puppy1 << "doggo" << "woof";
    EXPECT_THROW(throw Puppy1, TCustomException1);
    TCustomException2 Puppy2;
    Puppy2 << "abcde";
    EXPECT_THROW(throw Puppy2, TCustomException2);
}
int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

