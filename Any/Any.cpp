#include <iostream>
#include <typeindex>
#include <unordered_map>
#include <memory>

class Any {
public:
    template<typename T>
    Any(const T& value) {
        data = std::make_shared<Holder<T> >(value);
    }

    template<typename T>
    T get() const {
        if (data->type() == typeid(T)) {
            auto holder = std::static_pointer_cast<Holder<T> >(data);
            return holder->get();
        } else {
            throw std::runtime_error("Invalid type");
        }
    }

private:
    struct BaseHolder {
        virtual ~BaseHolder() {}
        virtual const std::type_info& type() const = 0;
    };

    template<typename T>
    class Holder : public BaseHolder {
    public:
        Holder(const T& value) : data(value) {};

        const std::type_info& type() const {
            return typeid(data);
        }

        T get() const {
            return data;
        }
    private:
        T data;
    };

    std::shared_ptr<BaseHolder> data;
};

int main() {
    Any a(5);
    std::cout << a.get<int>() << std::endl; // 5

    try {
        std::cout << a.get<std::string>() << std::endl; // error
    } catch (const std::exception& e) {
        std::cout << e.what() << std::endl;
    }

    return 0;
}

