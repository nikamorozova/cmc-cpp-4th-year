#include <map>
#include <iostream>
#include <chrono>


int main() {
    // FIRST (insert min element)
    std::map<int, int> test;
    auto inserted = test.insert({0, 1}).first;
    for (int i = 1; i < 20000; i++) {
        test.insert({i, i});
    }

    auto start = std::chrono::high_resolution_clock::now();
    for(int iter = 0; iter < 50000; iter++) {
        test.insert({0, 1});
    }
    auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();

    // -----> TIME WITHOUT HINT <-----
    // 0.0127705s
    std::cout << "time without hint: " << duration / 1e9 << "s\n";

    
    auto s_start = std::chrono::high_resolution_clock::now();
    for(int iter = 0; iter < 50000; iter++) {
        test.insert(inserted, {0, 1});
    }
    auto s_end = std::chrono::high_resolution_clock::now();
    auto s_duration = std::chrono::duration_cast<std::chrono::nanoseconds>(s_end - s_start).count();

    // -----> TIME WITH HINT <-----
    // 0.00540517s
    std::cout << "time with hint (in sec): " << s_duration / 1e9 << "s\n";

    // SECOND (initialize in increasing order)
    // WITHOUT HINT
    auto t_start = std::chrono::high_resolution_clock::now();
    for(int iter = 0; iter < 250; iter++) {
        std::map<int, int> test;
        for (int i = 1; i < 20000; i++) {
            test.insert({i, i});
        }
    }
    auto t_end = std::chrono::high_resolution_clock::now();
    auto t_duration = std::chrono::duration_cast<std::chrono::nanoseconds>(t_end - t_start).count();

    // -----> TIME without HINT <-----
    // 2.24269s

    std::cout << "time without hint: " << t_duration / 1e9 << "s\n";

    // WITH HINT

    auto new_start = std::chrono::high_resolution_clock::now();
    for(int iter = 0; iter < 250; iter++) {
        std::map<int, int> test;
        auto inserted = test.insert({0, 1}).first;
        for (int i = 1; i < 20000; i++) {
            inserted = test.insert(inserted, {i, i});
        }
    }

    auto new_end = std::chrono::high_resolution_clock::now();
    auto new_duration = std::chrono::duration_cast<std::chrono::nanoseconds>(new_end - new_start).count();

    // -----> TIME with HINT <-----
    // 1.70522s
    std::cout << "time with hint (in sec): " << new_duration / 1e9 << "s\n";


}